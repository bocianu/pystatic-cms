###define:blog_start###
<div class='news'><hr>
###define:blog_end###
</div>
###define:blog_item_start###  
<div class='news_item'>
<div class='news_head'><date>###blog:date###</date> / ###blog:author###</div>
###define:blog_item_end###  
</div><hr>
###define:pager_start###
<div id='pager'>
###define:pager_end###
</div>
###enddef###

###include:header.tpl###
###blog:pager###
###blog:feed###
###blog:pager###
###include:footer.tpl###
