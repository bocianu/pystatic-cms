###define:menu_start###
<ul id='menu'>
###define:menu_end###
</ul>
###define:menu_item_start###  
<li><a href='###item:url###'></a><div>###item.name###</div>
###define:menu_item_end###  
</li>
###define:submenu_start###
<ul class='submenu'>
###define:submenu_end###
</ul>
###define:submenu_item_start###  
<li><a href='###item:url###'>###item.name###</a>
###define:submenu_item_end###  
</li>
###enddef###