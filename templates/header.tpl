<html><head><title>###config:title###</title>
<meta http-equiv='Content-Type' content='text/html;charset=ISO-8859-2'>
<link rel='ICON' href='FAVICON.ICO' sizes='16x16'>
<link href='https://fonts.googleapis.com/css?family=Noto+Sans&display=swap' rel='stylesheet'>
<link rel='stylesheet' type='text/css' href='STYLES.CSS'>
<script type='text/javascript' src='https://code.jquery.com/jquery-latest.min.js'></script>
<body>
<div id="lb-back"><div id="lb-img"></div></div>
<div id='topbar'>
    <div id='titlebar' class='title'></div>
    <div id='titletext' class='title'><span class='atarired'>ATARI</span> 130XE Web-Server</div>
    ###menu:mainmenu###
</div>
<div id='main'>
