#!/usr/local/bin/python
# coding: utf-8
import re
import json
import os
import glob
import shutil
import markdown
import math

sourceDir = 'sources/'
templateDir = 'templates/'
outputDir = 'output/'

outFiles = []

def throwErr(msg, err):
    print "****** ERROR !!!"
    print msg
    if err is not None:
        print err
    raw_input("Press Enter to continue...")        
    exit()

def addExtension(name, extension):
    return name + "." + extension        
    
def getDirective(placeholder):
    return re.split(':',placeholder.strip('#'))

def getPlaceholders(line):
    return re.findall( r'###[a-zA-Z0-9\.:_]+###' ,line)

def getDefines(fileName):
    defines = {}
    with open(fileName) as tpl:
        defBody = ''
        defineStarted = False
        defineName = ''
        copyContent = True
        for line in tpl:

            copyContent = defineStarted

            for placeholder in getPlaceholders(line):
                directive = getDirective(placeholder)
                if directive[0] == 'define' :
                    if defineStarted:
                        defines[defineName] = defBody
                    defineStarted = True
                    defineName = directive[1]
                    defBody = ''
                    copyContent = False
                
                elif directive[0] == 'enddef' :
                    defines[defineName] = defBody
                    defineStarted = False
                    copyContent = False
            
            if copyContent:
                defBody = defBody + line
            
    return defines

def validateFileEntry(fileEntry):
    if not 'name' in fileEntry:
        throwErr("fileEntry without name", fileEntry)
    newEntry = fileEntry.copy()
    if not 'filename' in newEntry:
        newEntry["filename"] = newEntry["name"].upper()
    else:
        newEntry["filename"] = newEntry["filename"].upper()
    if not 'source' in newEntry:
        newEntry["source"] = addExtension(newEntry["name"], config['sourceFileExtension']).lower()
    else:
        newEntry["source"] = addExtension(newEntry["source"], config['sourceFileExtension']).lower()
    if not 'template' in newEntry:
        newEntry["template"] = config['defaultTemplate']

    return newEntry

def parseMenuItem(itemTpl, menuItem):
    url = addExtension(menuItem['filename'], config['outputFileExtension'])
    return itemTpl.replace('###item:url###', url).replace('###item.name###', menuItem['name'])

def parseSubmenu(menu, defines, parent):
    content = ''
    for menuEntry in menu:
        if 'submenu' in menuEntry:
            
            content += parseMenuItem(defines['submenu_item_start'], menuEntry)
            content += defines['submenu_start']
            content += parseSubmenu(menuEntry['submenu'], defines, parent)
            content += defines['submenu_end']
            content += defines['submenu_item_end']
        else:
            if "name" in parent and parent["name"] == 'index':
                transformToFile(menuEntry)
            entry = validateFileEntry(menuEntry)
            content += parseMenuItem(defines['submenu_item_start'], entry)
            content += defines['submenu_item_end']
    return content

def parseMenu(menuName, parent):
    content = ''
    if not menuName in config["menu"]:
        print 'menu not found:', menuName
    else:
        menu = config["menu"][menuName]
        defines = getDefines(templateDir + menu['template'])

        content += defines['menu_start']

        for menuEntry in menu['tree']:
            if 'submenu' in menuEntry:
                content += parseMenuItem(defines['menu_item_start'], menuEntry)
                content += defines['submenu_start']
                content += parseSubmenu(menuEntry['submenu'], defines, parent)
                content += defines['submenu_end']
                content += defines['menu_item_end']
            else:
                if "name" in parent and parent["name"] == 'index':
                    transformToFile(menuEntry)
               
                menuEntry = validateFileEntry(menuEntry)
                content += parseMenuItem(defines['menu_item_start'], menuEntry)
                content += defines['menu_item_end']

        content += defines['menu_end']
    return content

def parseBlog(fileEntry, parent):
    tplSource = templateDir + fileEntry["template"]
    defines = getDefines(tplSource)
    template = parseTpl(fileEntry, parent)
    paging = 5
    if 'paging' in fileEntry:
        paging = fileEntry['paging']
    blogFiles = glob.glob(sourceDir + fileEntry['blog'] + '/*')
    allItems = []
    pages = []

    for f in blogFiles:
        itemdefines = getDefines(f)
        with open(f, mode = 'r') as source:
            md = ''
            defineStarted = False
            for line in source:
                copyContent = not defineStarted
                for placeholder in getPlaceholders(line):
                    directive = getDirective(placeholder)
                    if directive[0] == 'define' :
                        defineStarted = True
                        copyContent = False
                    
                    elif directive[0] == 'enddef' :
                        defineStarted = False
                if copyContent:
                    md += line
            content = markdown.markdown(md.decode('utf-8'))
        allItems.append({'defines': itemdefines, 'content': content})

    def sortByDate(item):
        return item['defines']['date']
    
    allItems.sort(key = sortByDate, reverse = True)        

    itemCount = len(allItems)
    pagesCount = int(math.ceil(itemCount / float(paging)))
    itemPtr = 0

    for pageNum in range(pagesCount):

        feed = defines['blog_start']
        for itemOnPage in range(paging):
            item = allItems[itemPtr]
            feed += defines['blog_item_start']
            itembody = item['content']
            
            feed += itembody
            feed += defines['blog_item_end']
            for placeholder in getPlaceholders(feed):
                    directive = getDirective(placeholder)
                    if directive[0] == 'blog':
                        if directive[1] in item['defines']:
                            feed = feed.replace(placeholder ,item['defines'][directive[1]])

            itemPtr += 1
            if itemPtr == itemCount:
                break
        feed += defines['blog_end']
        

        pager = ''
        if pagesCount > 1:

            pager = defines['pager_start']
            for page in range(pagesCount):
                if page == pageNum:
                    pager += "<a href='#' class='current'>" + str(page + 1) + "</a>"
                else:
                    pager += "<a href='" + getPageFileName(fileEntry['filename'],page) + "'>" + str(page + 1) + "</a>"
            pager += defines['pager_end']

        page = template.replace('###blog:feed###', feed).replace('###blog:pager###', pager)

        pages.append(page)

    return pages

def parseTpl(fileEntry, parent):
    tplSource = templateDir + fileEntry["template"]
    with open(tplSource) as tpl:
       outfile = ''
       defineStarted = False
       copyContent = True
       for line in tpl:
            if not defineStarted:
                copyContent = True
            for placeholder in getPlaceholders(line):
                content = ''
                directive = getDirective(placeholder)

                if directive[0] == 'define' :
                    defineStarted = True
                    copyContent = False

                elif directive[0] == 'enddef' :
                    defineStarted = False

                elif directive[0] == 'source' :
                    fileSource = sourceDir + fileEntry["source"]
                    if not os.path.exists(fileSource):
                        throwErr("file source not found", fileSource )
                    with open(fileSource, mode = 'r') as source:
                        md = source.read().decode('utf-8')
                        content = markdown.markdown(md)

                elif directive[0] == 'include' :
                    content = parseTpl({ "template" : directive[1]}, parent)
    
                elif directive[0] == 'config' :
                    content = config[directive[1]]

                elif directive[0] == 'blog' :
                    content = placeholder

                elif directive[0] == 'menu' :
                    content = parseMenu(directive[1], parent)

                else:
                    print '**** unknown directive:', directive[0], directive
                
                line = line.replace(placeholder, content,1)

            if copyContent:
                try:
                    outfile += line #.decode('utf-8','ignore')
                except Exception as error:
                    print '******error in:', fileEntry
                    print line

                    throwErr("decoding error:", error)
    return outfile

def getPageFileName(filename, page):
    filenamePostfix = ''
    if page > 0:
        filenamePostfix = str(page + 1)
    return addExtension(filename + filenamePostfix, config['outputFileExtension'])


def transformToFile(fileEntry):
    entry = validateFileEntry(fileEntry)
    print '<<< Parsing:', entry['name'].encode('utf-8'), '<-', entry['source']
    if 'blog' in entry:
        htmlDocs = parseBlog(entry, entry)
    else:
        htmlDocs = [parseTpl(entry, entry)]
    cnt = 0
    for htmlDoc in htmlDocs:
        filename = outputDir + getPageFileName(entry['filename'], cnt)
        isoDoc = htmlDoc.encode('iso-8859-2','ignore')
        outFiles.append({ 'filename': filename, 'content': isoDoc})
        cnt += 1


def writeOutFiles():
    for ofile in outFiles:    
        with open(ofile['filename'], 'w') as html:
            print ">>> Writing file:", ofile['filename'], 'size:', len(ofile['content']), 'bytes'
            html.write(ofile['content'])
            html.close()

####################################################################

print "*** PyStatic - CMS builder *** by bocianu"
print ">>> Loading config file"
try:
    with open("config.json", "r") as read_file:
        configdata = read_file.read()
        config = json.loads(configdata)
except Exception as error:
    throwErr("!!! error opening config.json file:", error)

print ">>> Checking default template"
if config["defaultTemplate"] is not None:        
    if not os.path.exists(templateDir + config["defaultTemplate"]):
        throwErr("default Template not found", config["defaultTemplate"] )
        
print "*** SITE RENDERING STARTED: ***"
transformToFile(config["index"])

print "*** Checking file sizes"
for file in outFiles:    
    if len(file['content']) > config['singleFileSizeLimit']:
        throwErr("file too big:", file['filename'])

print '### Deleting all files in folder', outputDir
oldfiles = glob.glob(outputDir + '*')
for f in oldfiles:
    os.remove(f)

print '>>> Copying all static files'
staticfiles = glob.glob('static/*')
for f in staticfiles:
    shutil.copy(f, outputDir)

writeOutFiles()

print "*** SUCCESS !!!"

raw_input("Press Enter to continue...")